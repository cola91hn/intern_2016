package com.a3tsoft.datingarround.fragments;

import android.view.View;

import com.a3tsoft.datingarround.R;
import com.a3tsoft.datingarround.base.BaseFragment;

/**
 * Created by minhkiet on 22/11/2016
 */

public class InviteUserListFragment extends BaseFragment{

    public static InviteUserListFragment newInstance() {
        InviteUserListFragment fragment = new InviteUserListFragment();
        return fragment;
    }

    public InviteUserListFragment() {}

    @Override
    public int getLayoutIntance() {
        return R.layout.fragment_invite_user_list;
    }

    @Override
    public void initControls(View view) {

    }
}
