package com.a3tsoft.datingarround;

import com.a3tsoft.datingarround.base.BaseActivity;
import com.a3tsoft.datingarround.fragments.HomeFragment;
import com.a3tsoft.datingarround.utils.AppUtils;

public class MainActivity extends BaseActivity {

    @Override
    public int getLayoutIntance() {
        return R.layout.activity_main;
//        16.0666435,108.2109043 <~~~ current location lat,long
    }

    @Override
    public void initControls() {
        AppUtils.addNewFragment(this, R.id.container, HomeFragment.newInstance());
    }
}
