package com.a3tsoft.datingarround.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by tungle on 15/11/2016
 */
public abstract class BaseFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutIntance(), null);
        initControls(view);
        return view;
    }

    public abstract int getLayoutIntance();
    public abstract void initControls(View view);
}
