package com.a3tsoft.datingarround.fragments;

import android.content.Intent;
import android.graphics.Typeface;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.a3tsoft.datingarround.R;
import com.a3tsoft.datingarround.activity.UserListActivity;
import com.a3tsoft.datingarround.base.BaseFragment;

/**
 * Created by tungle on 15/11/2016
 */
public class HomeFragment extends BaseFragment {
    TextView tvMeetMe, tvDate;

    private Button btnStartDating;

    public static HomeFragment newInstance() {
        HomeFragment fragment = new HomeFragment();
        return fragment;
    }

    @Override
    public int getLayoutIntance() {
        return R.layout.fragment_home;
    }

    @Override
    public void initControls(View view) {
        btnStartDating = (Button) view.findViewById(R.id.fragment_home_btn_start_dating);
        tvMeetMe = (TextView) view.findViewById(R.id.fragment_home_tv_meet_me);
        tvDate = (TextView) view.findViewById(R.id.fragment_home_tv_date);
        Typeface faceBook = Typeface.createFromAsset(getActivity().getAssets(), "fonts/AvenirLTStd-Book.otf");
        Typeface faceRoman = Typeface.createFromAsset(getActivity().getAssets(), "fonts/AvenirLTStd-Roman.otf");
        btnStartDating.setTypeface(faceRoman);
        tvDate.setTypeface(faceBook);
        tvMeetMe.setTypeface(faceBook);
        btnStartDating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = UserListActivity.newIntent(getActivity());
                startActivity(intent);
            }
        });
    }
}
