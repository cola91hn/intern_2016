package com.a3tsoft.datingarround.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by tungle on 15/11/2016
 */
public abstract class BaseActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutIntance());
        initControls();
    }

    public abstract int getLayoutIntance();
    public abstract void initControls();
}
