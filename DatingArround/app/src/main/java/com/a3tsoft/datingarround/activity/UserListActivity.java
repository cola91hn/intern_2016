package com.a3tsoft.datingarround.activity;

import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.Button;

import com.a3tsoft.datingarround.R;
import com.a3tsoft.datingarround.base.BaseActivity;
import com.a3tsoft.datingarround.fragments.InviteUserListFragment;
import com.a3tsoft.datingarround.fragments.NearbyUserFragment;
import com.a3tsoft.datingarround.utils.AppUtils;

public class UserListActivity extends BaseActivity implements View.OnClickListener {

    private Button btnNearbyList, btnInviteList;

    public static Intent newIntent(Context context) {
        Intent intent = new Intent(context, UserListActivity.class);
        return intent;
    }

    @Override
    public int getLayoutIntance() {
        return R.layout.activity_user_list;
    }

    @Override
    public void initControls() {
        AppUtils.addNewFragment(this, R.id.container, NearbyUserFragment.newInstance());

        btnNearbyList = (Button) findViewById(R.id.btn_nearby);
        btnNearbyList.setOnClickListener(this);

        btnInviteList = (Button) findViewById(R.id.btn_invite);
        btnInviteList.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v == btnNearbyList) {
            AppUtils.showNewFragmentWithoutBackStack(this,
                    R.id.container, NearbyUserFragment.newInstance());
        }

        if (v == btnInviteList) {
            AppUtils.showNewFragmentWithoutBackStack(this,
                    R.id.container, InviteUserListFragment.newInstance());
        }
    }
}
