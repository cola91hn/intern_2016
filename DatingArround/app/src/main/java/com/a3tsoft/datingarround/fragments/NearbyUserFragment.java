package com.a3tsoft.datingarround.fragments;

import android.view.View;

import com.a3tsoft.datingarround.R;
import com.a3tsoft.datingarround.base.BaseFragment;

/**
 * Created by minhkiet on 22/11/2016.
 */

public class NearbyUserFragment extends BaseFragment {

    public static NearbyUserFragment newInstance() {
        NearbyUserFragment fragment = new NearbyUserFragment();
        return fragment;
    }

    public NearbyUserFragment() {}

    @Override
    public int getLayoutIntance() {
        return R.layout.fragment_nearby_user_list;
    }

    @Override
    public void initControls(View view) {

    }
}
